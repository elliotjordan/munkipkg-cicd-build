# munkipkg-cicd-build

A demonstration of using GitLab CI/CD to build [MunkiPkg](https://github.com/munki/munki-pkg) projects into Apple installer packages automatically.

- `munkipkg_build.sh`: The script executed on the macOS GitLab runner that builds the packages.
- `.gitlab-ci.yml`: The CI/CD configuration that triggers the script when commits are pushed to the `main` branch.
- `munki_kickstart`, `SuppressSetupAssistant`, and `TurnOffBluetooth`: Example projects from the MunkiPkg repository used for demonstration purposes.

For more details, see this post: https://www.elliotjordan.com/posts/munkipkg-03-gitlab-ci/
