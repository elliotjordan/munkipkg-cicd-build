#!/bin/sh
# This script is referenced by .gitlab-ci.yml and uses
# MunkiPkg to build projects in this repository.
MUNKIPKG="/usr/local/bin/munkipkg"
for proj in */build-info.*; do
    python3 $MUNKIPKG "$(dirname "$proj")" || exit 1
done
